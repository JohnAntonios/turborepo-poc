import "@unified-commerce/components/dist/index.css";

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
