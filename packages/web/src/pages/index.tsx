import { Button } from "@unified-commerce/components";
import { useIsomorphicLayoutEffect } from "@unified-commerce/utils";

export default function Index() {
  useIsomorphicLayoutEffect(() => {
    console.log("The index page!");
  }, []);

  return (
    <>
      <h1>Home page</h1>
      <Button disabled>Unable to click</Button>
      <Button>Click</Button>
    </>
  );
}
