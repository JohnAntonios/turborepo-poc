# Turborepo POC

A proof of concept on turborepo with Yarn workspaces.

Framework/s: React + NextJS.

Bundler: [tsup](https://tsup.egoist.sh/).

- `@unified-commerce/components`: React components
- `@unified-commerce/eslint-preset`: ESLint preset
- `@unified-commerce/tsconfig`: shared `tsconfig.json`s used throughout the monorepo
- `@unified-commerce/utils`: shared React utilities
- `@unified-commerce/web`: A site powered by [Next.js](https://nextjs.org)

## Getting started

1. Run `yarn install`

2. Run `yarn dev` - this hot reloads.

3. Go to http://localhost:3002

## Outcome

Amazing.

```bash
@unified-commerce/web:build: Page                                       Size     First Load JS
@unified-commerce/web:build: ┌ ○ /                                      443 B            72 kB
@unified-commerce/web:build: ├   /_app                                  0 B            71.5 kB
@unified-commerce/web:build: └ ○ /404                                   193 B          71.7 kB
@unified-commerce/web:build: + First Load JS shared by all              71.5 kB
@unified-commerce/web:build:   ├ chunks/framework-c4190dd27fdc6a34.js   42 kB
@unified-commerce/web:build:   ├ chunks/main-a122f0795c409f78.js        28.2 kB
@unified-commerce/web:build:   ├ chunks/pages/_app-6344a38b3d24d48e.js  508 B
@unified-commerce/web:build:   ├ chunks/webpack-514908bffb652963.js     770 B
@unified-commerce/web:build:   └ css/e760cc077aeab8f3.css               90 B
```
